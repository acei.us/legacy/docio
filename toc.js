// Unminified version
function generatetoc() {
    let ToC = "<ol>"
    let htitle, link, curElmt, newLine 

    $("h1").each(function() {
        curElmt = $(this)
        htitle = curElmt.text() 
        link = "#" + curElmt.attr("id")
        newLine = "<li><a href='" + link +"'>" + htitle +"</a><br/></li>"
        ToC += newLine
        console.log('completed cycle for h1')
    })
    $("h2").each(function() {
        curElmt = $(this)
        htitle = curElmt.text() 
        link = "#" + curElmt.attr("id")
        newLine = "<li><a href='" + link +"'>" + htitle +"</a><br/></li>"
        ToC += newLine
        console.log('completed cycle')
    })
    $("h3").each(function() {
        curElmt = $(this)
        htitle = curElmt.text() 
        link = "#" + curElmt.attr("id")
        newLine = "<li><a href='" + link +"'>" + htitle +"</a><br/></li>"
        ToC += newLine
        console.log('completed cycle')
    })
    $("h4").each(function() {
        curElmt = $(this)
        htitle = curElmt.text() 
        link = "#" + curElmt.attr("id")
        newLine = "<li><a href='" + link +"'>" + htitle +"</a><br/></li>"
        ToC += newLine
        console.log('completed cycle')
    })
    $("h5").each(function() {
        curElmt = $(this)
        htitle = curElmt.text() 
        link = "#" + curElmt.attr("id")
        newLine = "<li><a href='" + link +"'>" + htitle +"</a><br/></li>"
        ToC += newLine
        console.log('completed cycle')
    })
    $("h6").each(function() {
        curElmt = $(this)
        htitle = curElmt.text() 
        link = "#" + curElmt.attr("id")
        newLine = "<li><a href='" + link +"'>" + htitle +"</a><br/></li>"
        ToC += newLine
        console.log('completed cycle')
    })

    ToC += "</ol>"
    $("#index").html(ToC)
    console.log(ToC)
}