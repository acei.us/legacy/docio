A simple static content wiki system.

# Notes & Limitations
## Links
To link to another page in DocIO, you must use this syntax:  
```[Link Text](./?title=Page_Name)```  
Here is an [example](./?title=index_2).
This section should be disreguarded if you are using the rewrite engine.
## Page Names
Currently, DocIO does not support spaces in page names. If you want a space to render in the name of your page, use an `_` instead.

# Installation
1. Download a copy of the main branch of the DocIO [Git Repo](https://gitlab.com/acei.us/experiments/docio).
2. Unzip the folder, and drag the contents into any webserver. 
3. Open the web folder in your browser, if it works you should see the [homescreen](./). Congrats, you now have a working copy of DocIO!
4. You can now configure your copy of DocIO. Make sure to save changes of your config file, as you may need to make them again later.
Optional Steps:
* Enable the rewrite engine to remove the ugly `?title=Page` from URLs
* Create a custom 404 page for DocIO using an .htaccess file

# Useage
## Creating and Editing pages
DocIO uses Markdown to parse it's content. If you don't know Markdown, [here is a cheatsheet](https://www.markdownguide.org/cheat-sheet).  
In order to create new pages, simply create a file named what you want your page to be named in the data folder, appended with `.md` and with underscores in place of spaces.
## Configuration
* `config.version` - The version of the config format. Should not be changed unless specified during upgrade. 
  * Type: Integer
  * Default: `0.1`
* `config.name` - Name of the website, used in window title, and as homepage name.
  * Type: String
  * Default: `DocIo`
* `config.data` - Path to data folder. MUST Contain trailing `/`. For example, `./data/`, or if your project has a docs folder containing markdown, `https://github.com/example/example/docs`
* `config.selector` - Used in URLs to denote page names. Not a good idea to change.
  * Type: String
  * Default: `title`
* `config.style` - Category for CSS values
  * `config.style.font` - CSS `font-family` value.
    * Type: CSS Value
    * Default: `sans-serif`
  * `config.style.color` - CSS `color` value.
      * Type: CSS Value
      * Default: `#111`
  * `config.style.background` - CSS `background` value.
      * Type: CSS Value
      * Default: `#fff`
  * `config.style.menu_open` - Should the Navigation Menu be open by default?
      * Type: Boolean
      * Default: `false`