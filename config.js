config = {
    version: '0.2', // Do not change unless you know what you are doing.

    name: 'DocIo',
    data: 'data/',
    selector: 'title',
    style: {
        font: 'sans-serif',
        color: '#111',
        background: '#fff',
        menu_open: false,
        toc_open: true,
    }
}